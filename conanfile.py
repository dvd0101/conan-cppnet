from conans import ConanFile, CMake
from conans.tools import replace_in_file


class CppnetConan(ConanFile):
    name = "cpp-netlib"
    version = "git"
    url = "https://gitlab.com/cinghiale/conan-cppnet"
    license = "Boost Software License - Version 1.0"
    settings = "os", "compiler", "build_type", "arch"
    requires = "Boost/1.60.0@lasote/stable", "OpenSSL/1.0.2h@lasote/stable"

    options = {"shared": [True, False], "fPIC": [True, False], "revision": "ANY"}
    default_options = "shared=False", "fPIC=True", "revision=\"a97ab01cae4c3971edd36127a9067037072b18a1\""
    generators = "cmake"

    def source(self):
        repo = "https://github.com/cpp-netlib/cpp-netlib.git"
        self.run("git clone %s" % repo)
        self.run("cd cpp-netlib && git checkout %s" % self.options.revision)

    def build(self):
        cmake = CMake(self.settings)
        flags = [
            "-DCPP-NETLIB_BUILD_TESTS=OFF",
            "-DCPP-NETLIB_BUILD_EXAMPLES=OFF",
        ]
        if self.options.shared:
            flags.append("-DBUILD_SHARED_LIBS=ON")
        if self.settings.os != "Windows" and self.options.fPIC:
            flags.append("-DCMAKE_POSITION_INDEPENDENT_CODE=TRUE")

        replace_in_file(
            "cpp-netlib/CMakeLists.txt",
            "project(CPP-NETLIB)",
            """project(CPP-NETLIB)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()""")
        self.run('cmake cpp-netlib %s %s' % (cmake.command_line, " ".join(flags)))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.hpp", dst="include/boost", src="cpp-netlib/boost")
        self.copy("*.ipp", dst="include/boost", src="cpp-netlib/boost")
        self.copy("*.hpp", dst="include/", src="cpp-netlib/deps/asio/asio/include")
        self.copy("*.ipp", dst="include/", src="cpp-netlib/deps/asio/asio/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["cppnetlib-uri", "cppnetlib-client-connections", "cppnetlib-server-parsers"]
        self.cpp_info.cppflags = ["-std=c++11"]
        self.cpp_info.defines = ["BOOST_NETWORK_ENABLE_HTTPS"]
