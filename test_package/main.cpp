#include <boost/network/protocol/http/client.hpp>
#include <string>

int main(int argc, char *argv[]) {
    using namespace boost::network;

    http::client client;
    http::client::request request(std::string("http://localhost/"));
    request << header("Connection", "close");

    return 0;
}
